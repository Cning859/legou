import request from '@/api/request'

function  helperaction(data){
    return request({
        method:"get",
        url:"/search/helperaction",
        data
    })
}

function  history(data){
    return request({
        method:"get",
        url:"/search/indexaction",
        data
    })
}
function  add(data){
    return request({
        method:"post",
        url:"/search/addhistoryaction",
        data
    })
}
function  clean(data){
    return request({
        method:"post",
        url:"/search/clearhistoryAction",
        data
    })
}



export {
    helperaction,
    history,
    add,
    clean
}