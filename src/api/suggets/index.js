import request from '@/api/request'

function  submit(data){
    return request({
        method:"post",
        url:"/feedback/submitAction",
        data
    })
}
export {
    submit
}