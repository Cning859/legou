import request from "@/api/request"

// 请求首页数据
function  detailPage(data){
    return request({
        method:"get",
        url:"/goods/detailaction",
        data
    })
}
function  addGoods(data){
    return request({
        method:"post",
        url:"/cart/addCart",
        data,
    })
}

function  addCollect(data){
    return request({
        method:"post",
        url:"/collect/addcollect",
        data,
    })
}
export {
    detailPage,
    addGoods,
    addCollect
}