import request from "@/api/request"

// 收获地址
function  addressAdd(data){
    return request({
        method:"post",
        url:"/address/saveAction",
        data
    })
}
function  addressDetail(data){
    return request({
        method:"get",
        url:"/address/detailAction",
        data
    })
}




export {
    addressAdd,
    addressDetail

}