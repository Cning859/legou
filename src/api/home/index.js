import request from "@/api/request"

// 请求首页数据
function  home(data){
    return request({
        method:"get",
        url:"/index/index",
        data
    })
}
export {
    home
}