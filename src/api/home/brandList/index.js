import request from "@/api/request"

// 请求首页数据
function  brandList(data){
    return request({
        method:"get",
        url:"/brand/listaction",
        data
    })
}
export {
    brandList
}