import request from "@/api/request"

// 请求首页数据
function  detailaction(data){
    return request({
        method:"get",
        url:"/brand/detailaction",
        data
    })
}
export {
    detailaction
}