import request from "@/api/request"

// 请求首页数据
function  carList(data){
    return request({
        method:"get",
        url:"/cart/cartList",
        data
    })
}

// 请求首页数据
function  carDel(data){
    return request({
        method:"get",
        url:"/cart/deleteAction",
        data
    })
}


export {
    carList,carDel
}