import request from "@/api/request"

// 请求首页数据
function  carList(data){
    return request({
        method:"get",
        url:"/cart/cartList",
        data
    })
}

// 请求首页数据
function  address(data){
    return request({
        method:"get",
        url:"/order/detailAction",
        data
    })
}
// 请求首页数据
function  submit(data){
    return request({
        method:"post",
        url:"/order/submitAction",
        data
    })
}
function  getListAction(data){
    return request({
        method:"get",
        url:"/address/getListAction",
        data
    })
}


export {
    carList,
    address,
    submit,
    getListAction
}