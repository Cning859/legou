import request from '@/api/request'

function  topic(data){
    return request({
        method:"get",
        url:"/topic/listaction",
        data
    })
}
export {
    topic
}