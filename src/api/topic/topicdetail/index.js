import request from '@/api/request'

function  topicdetail(data){
    return request({
        method:"get",
        url:"/topic/detailaction",
        data
    })
}
export {
    topicdetail
}