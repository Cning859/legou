import request from "@/api/request"

// 请求首页数据
function  categoryNav(data){
    return request({
        method:"get",
        url:"/category/indexaction",
        data
    })
}

function  categoryList(data){
    return request({
        method:"get",
        url:"/category/currentaction",
        data
    })
}



export {
    categoryNav,
    categoryList
}