import request from "@/api/request"

// 请求首页数据
function  categoryNav(data){
    return request({
        method:"get",
        url:"/category/categoryNav",
        data
    })
}

function  categoryList(data){
    return request({
        method:"get",
        url:"/goods/goodsList",
        data
    })
}


export {
    categoryNav,
    categoryList,
   
}