import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/home/index.vue'

Vue.use(VueRouter)

const routes = [
  {
    path:'/',
    redirect:'/home'
  },
  {
    path: '/home',
    name: 'Home',
    component: Home,
    meta:{
      isShow:true
    }
  },
  {
    path:'/home/city',
    name:'City',
    component:()=> import('../views/home/city/index.vue'),
    meta:{
      isShow:false
    }
  },
  {
    path:'/home/brandList',
    name:'BrandList',
    component:()=> import('../views/home/brandList/index.vue')
  },
  {
    path:'/home/brandList/detailaction',
    name:'Detailaction',
    component:()=> import('../views/home/brandList/detailaction/index.vue')
  },
  {
    path:'/home/goodsList',
    name:'GoodsList',
    component:()=> import('../views/home/goodsList/index.vue')
  },
  {
    path: '/topic',
    name: 'Topic',
    component: () => import( '../views/topic/index.vue'),
    meta:{
      isShow:true
    }
  },
  {
    path:'/topic/topicdetail/:ids',
    name:'TopicDetail',
    component:()=> import('../views/topic/topicdetail/index.vue')
  },
  {
    path:'/category',
    name:'Category',
    component:()=> import('../views/category/index.vue'),
    meta:{
      isShow:true
    }
  },
  {
    path:'/category/:ids',
    name:'CategoryGoods',
    component:()=> import('../views/category/goodsList/index.vue')
  },
  {
    path:'/car',
    name:'Car',
    component:()=>import('../views/car/index.vue'),
    meta:{
      isShow:true
    }
  },
  {
    path:'/car/pay',
    name:'Pay',
    component:() => import('../views/car/pay/index.vue')
  },
  {
    path:'/my',
    name:'My',
    component:()=> import('../views/my/index.vue'),
    meta:{
      isShow:true
    }
  },
  {
    path:'/detail',
    name:'Detail',
    component:() => import('../views/detail/index.vue')
  },
  {
    path:'/search',
    name:'Search',
    component:()=> import('../views/search/index.vue')
  },
  {
    path:'/collect',
    name:'Collect',
    component:() =>import('../views/collect/index.vue')
  },
  {
    path:'/address',
    name:'/Address',
    component:() =>import('../views/address/index.vue')
  },
  {
    path:'/address/add',
    name:'Add',
    component:() =>import('../views/address/add/index.vue')
  },
  {
    path:'/suggest',
    name:'Suggest',
    component:() =>import('../views/suggest/index.vue')
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
