module.exports = {
    css: {
        loaderOptions: {
          css: {},
          postcss: {
            plugins: [
              require('postcss-px2rem')({
                remUnit: 37.5
              })
            ]
          }
        }
    },
      devServer : {
          hot:true,
          open : true,
          port : 8080,
          host : "127.0.0.1"
      }
  
}